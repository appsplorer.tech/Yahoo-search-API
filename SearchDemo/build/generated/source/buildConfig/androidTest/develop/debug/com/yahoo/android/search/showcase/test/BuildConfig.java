/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.yahoo.android.search.showcase.test;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.yahoo.android.search.showcase.test";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "develop";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "1.0";
  // Fields from product flavor: develop
  public static final String developerAppId = "invalid";
}
